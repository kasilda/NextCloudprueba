Fuentes:

- https://docs.nextcloud.com/server/stable/admin_manual/installation/source_installation.html

- https://www.php.net/manual/en/extensions.alphabetical.php

# DUDAS NGINX

Hace falta crear un repositorio de NGINX para ajustarlo al Nextcloud

Ejemplo: https://git.sindominio.net/estibadores/nginx-omeka-s

Dockerfile: Yo no sé hacer el archivo de fastcgi_params que está en el directorio /etc/nginx/fastcgi_params Tampoco sé si es realmente necesario, aunque entiendo que sí para que el NGINX lea los archivos php a través de php-fpm.

# DUDAS NEXTCLOUD

No sé cómo se hace para que cada vez que hay una nueva actualización de Nextcloud se cargue automáticamente, por ahora está para descargar directamente la versión 22.2.0.

# DUDAS DOCKERFILE

1. como incluimos variables? como hacemos una variable en caso de que la firma (al descargar la imagen de nextcloud) no coincida de que el proceso se pare

# Comentarios comparando el Dockerfile y la configuracion en SD

1. Sobre la carpeta /var/www/nextcloud que aparece en el Dockerfile >> en lxc lo tenemos montado directamente en /home/nextcloud dentro hay /home/nextcloud/httpdocs y ambas carpetas tienen permisos rwxr-xr-x nextcloud:nextcloud
 - La necesitamos en /var/www? 
 - La queremos en /home?
2. En el Dockerfile crea la carpeta nextcloud en root y en lxc la tenemos en home y no en root
 - Puede ir en root? Porque?
 - Donde la necesitamos? 
 - Donde la queremos?