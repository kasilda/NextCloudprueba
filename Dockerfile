FROM registry.sindominio.net/php-fpm

# Instalar todos los módulos necesarios de PHP y programas necesarios para el script

RUN apt-get update
RUN apt-get -qy install php-mysql php-curl php-gd php-mbstring php-xml php-xmlrpc php-soap php-intl php-zip php-imagick php-json php-bz2
RUN apt-get install wget tar -y
RUN apt-get -y install nginx-full

# Crear la carpeta donde se va a descargar y usar el nextcloud

RUN mkdir /var/www/nextcloud
RUN chown -R www-data:www-data /var/www/nextcloud
WORKDIR /var/www/nextcloud

# Cómo hacer para que se actualice y descargue la última versión????

RUN wget https://download.nextcloud.com/server/releases/nextcloud-22.2.0.tar.bz2
RUN wget https://download.nextcloud.com/server/releases/nextcloud-22.2.0.tar.bz2.md5

# Verificar la firma y descomprimir el Nextcloud

RUN md5sum --check nextcloud-22.2.0.tar.bz2.md5
RUN tar xvjf nextcloud-22.2.0.tar.bz2

# Añadir los ficheros de configuración del nginx

COPY nextcloud.conf /etc/nginx/sites-available/nextcloud.conf
RUN rm /etc/nginx/sites-enabled/default
RUN ln -s /etc/nginx/sites-available/nextcloud.conf /etc/nginx/sites-enabled/nextcloud.conf

# Creamos la carpeta persistente para el MariaDB del nextcloud  y creamos un volumen para indicar luego la base de datos

RUN mkdir /nextcloud
VOLUME /nextcloud/

CMD /usr/sbin/nginx -g "daemon off; master_process off;"
