# Vamos a hacer un Dockerfile de NextCloud

Hay varios pasos que hay que dar. Tenemos un ejemplo de un servicio (Omeka) muy parecido que usa varios de los componentes que necesitamos en el Nextcloud.

Ejémplo Omeka: https://git.sindominio.net/estibadores/omeka-s

Hay varios componentes que necesita el Nextcloud para poder funcionar

	a) nginx (servidor web)
	b) mariadb (base de datos)
	c) php-fpm (lector de php para nginx)
	d) nextcloud


